package com.spring.entity;

/**
 * This class creates address object which has Employee  address 
 * @author VARUN
 *
 */
public class Address {

//	declaring the properties
	private String city;
	private String state;
	private String country;
	
	/**
	 * creating getters and setters for employee address properties
	 * @return
	 */
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	
}
