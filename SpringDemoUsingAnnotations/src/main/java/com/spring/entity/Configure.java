package com.spring.entity;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*
 * This class is used to configure the bean classes dependency using configuration annotation
 */
@Configuration
public class Configure {
	/*
	 * This method is used to configure the employee bean into the container using
	 * bean annotation
	 * 
	 */
	@Bean
	public Employee employee() {
		// Creating object for employee to set the values
		Employee employee = new Employee();

		employee.setEmp_id(101);
		employee.setEmp_name("varun");

		// Creating object for Address to set the values
		Address address = new Address();

		address.setCity("hyderabad");
		address.setState("telangana");
		address.setCountry("India");

		employee.setAddress(address);

		return employee;

	}
}
