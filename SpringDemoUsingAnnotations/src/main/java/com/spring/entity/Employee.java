package com.spring.entity;

/**
 * This class creates employee object which has Employee properties
 * @author 	VARUN
 *
 */
public class Employee {

	//	declaring the properties
	private int emp_id;
	private String emp_name;
	
	private Address address;
	
	/**
	 * creating getters and setters for employee properties
	 * @return
	 */
	public int getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(int emp_id) {
		this.emp_id = emp_id;
	}
	public String getEmp_name() {
		return emp_name;
	}
	public void setEmp_name(String emp_name) {
		this.emp_name = emp_name;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	
	
	
}
