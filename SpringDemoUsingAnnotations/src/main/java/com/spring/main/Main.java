package com.spring.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.spring.entity.Address;
import com.spring.entity.Configure;
import com.spring.entity.Employee;

/**
 * This clas is used for getting output using spring annotation
 * @author Varun
 *
 */
public class Main {
	
	public static void main(String[] args) {
		
		//creating object for AnnotationConfigApplicationContext which implements ApplicationContext
		ApplicationContext context = new AnnotationConfigApplicationContext(Configure.class);
		
		//calling the getBean method for getting the output
		Employee emp=(Employee) context.getBean(Employee.class);
		
		System.out.println(emp.getEmp_id());
		System.out.println(emp.getEmp_name());
		
		
		//calling the getAddress method for getting the output
		Address address=emp.getAddress();
		
		System.out.println(address.getCity());
		System.out.println(address.getState());
		System.out.println(address.getCountry());
		
		//closing ApplicationContext
		((AbstractApplicationContext) context).close();
	}

}
