package com.abc.main;

import com.abc.entity.Product;
import com.abc.service.ProductService;

public class Main {

	public static void main(String[] args) {
		Product product=new Product();
		product.setProductID("102");
		product.setProductName("samsung");
		product.setProductPrice(10000);
		product.setProductCategory("Mobile");
		
		 new ProductService().createProduct(product);
		 System.out.println("done");

	}

}
