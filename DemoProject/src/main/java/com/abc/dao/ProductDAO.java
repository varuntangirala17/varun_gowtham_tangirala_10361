package com.abc.dao;

//import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;


import com.abc.entity.Product;
import com.abc.util.HibernateUtil;


public class ProductDAO {
	public boolean create(Product product) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
//		Query query=session.createQuery("from product");
		session.save(product);
		System.out.println("product saved");
		txn.commit();
		session.close();
		return true;
	}
}
