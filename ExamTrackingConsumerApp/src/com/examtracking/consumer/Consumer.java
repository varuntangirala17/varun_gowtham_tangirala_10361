package com.examtracking.consumer;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;

import com.examtracking.bean.AdminLoginDetails;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * Servlet implementation class Consumer
 */
@WebServlet("/Consumer")
public class Consumer extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//initializing the variables
		String username = null;
		String password = null;
		
		
		JSONObject jsonObject = null;
		try {
			//getting the data from script file 
			jsonObject = new JSONObject(request.getParameter("loginData"));
			//converting the json object to string
			jsonObject.toString();
			//getting the credentials username and password
			username = jsonObject.getString("username");
			password = jsonObject.getString("password");

		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		//sending the data to consumer to check the details
		String result = new Consumer().doLoginRequest(username, password);
		
		//validating the credentials and operating 
		if(result.equalsIgnoreCase("login successful")) {
			response.sendRedirect("success.html");
		}else {
			response.sendRedirect("error.html");
		}
	}
	/**
	 * This method makes a Login request to the web service identified by respective
	 * url
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	public String doLoginRequest(String username, String password) {
		
		// preparing url
		String url = "http://localhost:8080/ExamTrackingServiceApplication/rest/adminfunctions/login";
		
		// creating empty client object
		Client client = Client.create();
		
		// passing url to the client object which returns web resource object
		WebResource resource = client.resource(url);
		
		// passing parameters
		resource = resource.queryParam("username", username);
		resource = resource.queryParam("password", password);
		
		// making request to web service
		ClientResponse res = resource.accept(MediaType.TEXT_PLAIN).post(ClientResponse.class);
		
		// getting entity from response
		String str = res.getEntity(String.class);
		
		return str;
	} // End of doLoginRequest


}
