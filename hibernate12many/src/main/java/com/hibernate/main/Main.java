package com.hibernate.main;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.hibernate.entity.UserInfo;
import com.hibernate.entity.Vehicle;
import com.hibernate.util.HibernateUtil;

/**
 * this class is used for demo purpose of criteria and one-one mapping
 * @author IMVIZAG
 *
 */
public class Main {
	public static void main(String[] args) {
		
		//setting attributes in employee class
			UserInfo e1=new UserInfo();
			e1.setUser_id(1);
		    e1.setUser_name("varun"); 
		    
		  //setting attributes in employee class
			UserInfo e2=new UserInfo();
			e2.setUser_id(2);
		    e2.setUser_name("shashank"); 
		    
		    //setting attributes in product class  
		    Vehicle vehicle=new Vehicle();    
		    vehicle.setVehicle_id(3);
		    vehicle.setVehicle_name("bike");
	
		   Vehicle vehicle2=new Vehicle();    
		   vehicle2.setVehicle_id(4);
		   vehicle2.setVehicle_name("car");
		   
		   Set s=new HashSet();
		   s.add(vehicle);
		   s.add(vehicle2);
		   
		   e1.setVehicles(s);
		   e2.setVehicles(s);
		   //geting sessionfactory from util factory to create session
			SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
			
			//invoking session to perfrom operations
			Session session=sessionFactory.openSession();
			
			//begin transaction to commit the values
			Transaction t=session.beginTransaction();
			
			//saving the product
			session.save(e1);
			session.save(e2);	
			session.save(vehicle);
			session.save(vehicle2);
			//commting the transaction
		    t.commit();
		    
		    //closing the session class
			session.close();

	}

}
