package com.hibernate.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="vehicle_tbl1")
public class Vehicle {
	@Id
	@Column
	private int vehicle_id;
	
	@Column
	private String vehicle_name;
	@ManyToMany(targetEntity=UserInfo.class,mappedBy="vehicles")
	private Set<UserInfo> userinfo;
	 
	public Set<UserInfo> getUserinfo() {
		return userinfo;
	}
	public void setUserinfo(Set<UserInfo> userinfo) {
		this.userinfo = userinfo;
	}
	public int getVehicle_id() {
	return vehicle_id;
	}
	public void setVehicle_id(int vehicle_id) {
	this.vehicle_id = vehicle_id;
	}
	public String getVehicle_name() {
	return vehicle_name;
	}
	public void setVehicle_name(String vehicle_name) {
	this.vehicle_name = vehicle_name;
	}
	

	}
