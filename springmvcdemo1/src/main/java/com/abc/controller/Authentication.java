package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
@SessionAttributes("data")
public class Authentication {
	
	@GetMapping("loginform")
	public String getLogin() {
		return "login";
	}

	@PostMapping("/login")
	public ModelAndView doLogin(@ModelAttribute LoginData data) {
		

		if(data.getEmail().equals("Admin")&&data.getPassword().equals("admin123")) {
			
			ModelAndView mav = new ModelAndView("registration","data",data);
			
			return mav;
		}
		else {
			ModelAndView mav = new ModelAndView("login_failure");

			return mav;
		}
		
	}
	
	@PostMapping("/register")
	public ModelAndView doRegister(@ModelAttribute UserData user) {
		ModelAndView mav = new ModelAndView("registration_success","user", user);
		
		return mav;
		
	
	}
}
