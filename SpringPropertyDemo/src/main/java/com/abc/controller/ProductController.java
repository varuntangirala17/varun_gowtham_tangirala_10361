package com.abc.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.abc.entity.Product;
import com.abc.service.ProductService;

@Controller
public class ProductController {
	//Taking product service object from spring
	@Autowired
	ProductService productService;
	
	
	public void setProductService(ProductService productService) {
		this.productService = productService;
	}
	
	public boolean saveProduct(Product product)
	{
		return productService.saveProduct(product);//calling method in productservice class
	}
	
}
