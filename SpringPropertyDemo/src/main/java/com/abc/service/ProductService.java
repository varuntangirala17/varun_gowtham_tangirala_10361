package com.abc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.dao.ProductDao;
import com.abc.entity.Product;

@Service
public class ProductService {
	//taking productdao object from spring
	@Autowired
	ProductDao productDao;

	public void setProductDao(ProductDao productDao) {
		this.productDao = productDao;
	}
	
	public boolean saveProduct(Product product)
	{
		return this.productDao.saveProduct(product);//calling method to save product in dao
	}
}
