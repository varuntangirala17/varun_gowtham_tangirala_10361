package com.abc.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {

	private static SessionFactory sessionFactory = hibernetConnection();
	private static SessionFactory hibernetConnection() {
		
		ServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
		MetadataSources data = new MetadataSources(registry); 
		Metadata metaData = data.getMetadataBuilder().build();
		
		sessionFactory = metaData.getSessionFactoryBuilder().build();
		
		return sessionFactory;
		
		
	}
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
		
}
