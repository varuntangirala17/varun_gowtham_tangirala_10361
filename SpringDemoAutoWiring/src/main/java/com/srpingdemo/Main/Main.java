package com.srpingdemo.Main;

import com.srpingdemo.bean.Address;
import com.srpingdemo.bean.Employee;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * This clas is used for getting output using spring annotation
 * @author 	varun
 *
 */
public class Main {

	public static void main(String[] args) {
		

		//creating object for AnnotationConfigApplicationContext which implements ApplicationContext
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:com/springdemo/config/Context.xml");
		//calling the getBean method for getting the output
		Employee emp=(Employee) context.getBean("emp");
	   
	   
	   System.out.println(emp.getEmpId());
	   System.out.println(emp.getEmpName());
	 //calling the getAddress method for getting the output
	   Address empAddress =emp.getAddr();
	   System.out.println(empAddress.getCity());
	   System.out.println(empAddress.getState());
	   

	}

}
