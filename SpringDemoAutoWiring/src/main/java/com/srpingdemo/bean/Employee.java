package com.srpingdemo.bean;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * This class creates employee object which has Employee properties
 * @author 	VARUN
 *
 */
public class Employee {

//	declaring the properties
	private int empId;
	private String empName;
	@Autowired
	private Address addr;
	/**
	 * creating getters and setters for employee properties
	 * @return
	 */
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public Address getAddr() {
		return addr;
	}
	public void setAddr(Address addr) {
		this.addr = addr;
	}

	
	
}
