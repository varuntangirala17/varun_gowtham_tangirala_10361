package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;

import com.abc.bean.UserDetails;

@Controller
public class AuthController {
	@Autowired
	private UserDetails user;

	@GetMapping("loginform")
	public String getLoginForm() {
		return "login";
	}
	
	@PostMapping("/login")
	public String doLogin(@RequestParam("email") String email,@RequestParam("pwd") String pwd,ModelMap map) {
		
		if(pwd.equals("123")) {
			map.addAttribute("myemail", user.getFirstName());
			return "login_success";
		}
		else {
			return "login_failure";
		}
		
	}
	
	@GetMapping("registerform")
	public String getRegisterForm() {
		return "registration";
	}
	
	@PostMapping("/login_success")
	public String doRegister(@ModelAttribute UserDetails user) {
		
		if(user.getPassword().length() < 3) {
		   return "registration_failure";	
		}
		else {
			return "registration_success";
		}
		
		
	}
	
	
}
