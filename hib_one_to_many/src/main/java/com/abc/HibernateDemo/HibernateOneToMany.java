package com.abc.HibernateDemo;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.hibernatedemo.entity.Cart;
import com.hibernatedemo.entity.Item;
import com.hibernatedemo.util.HibernateUtil;

public class HibernateOneToMany {

	public static void main(String[] args) {
		
		Item item = new Item();
		item.setItemname("Bru");
		item.setPrice(20);
		item.setQuantity(200);
		List<Item> itemsList = new ArrayList<Item>();
		itemsList.add(item);
		
		Cart cart = new Cart();
		cart.setItems(itemsList);
		cart.setTotal(item.getPrice());
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		session.save(cart);
		session.save(item);
		
		Transaction transaction =session.beginTransaction();
		transaction.commit();
	}

}
