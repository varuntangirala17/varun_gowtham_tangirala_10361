package com.student.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * This class used to establish connection with Database
 * @author BATCH-C
 *
 */
public class DBConnection {
	/***
	 * This method establish connection and return Connection Object
	 * @return Connection Obj
	 */
	
	public static Connection getDBConnection() {
		/**
		 * this method is used for connection with database 
		 */
		Connection con = null;
		try {
			// loading driver of mysql 
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			// connecting with database  for executing sql commands 
			 con = DriverManager.getConnection("jdbc:mysql://localhost:3306/server_sample", "root",
					"innominds");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return con;
	}
}
