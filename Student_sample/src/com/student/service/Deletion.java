package com.student.service;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.student.bean.StudentBean;
import com.student.dao.StudentDeletiondao;


/**
 * Servlet implementation class Deletion
 */
@WebServlet("/Deletion")
public class Deletion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Deletion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		//this method is used for collecting infromation from html page and send to studentdao class 
		String student_id=request.getParameter("id");
		int stud_id=Integer.parseInt(student_id);
		StudentBean student=StudentDeletiondao.deleteStudent(stud_id);
		if(student !=null) {
			student.getStud_id();
			student.getName();
			student.getAge();
			request.setAttribute("student",student);
			RequestDispatcher rd=request.getRequestDispatcher("delete.jsp");
			rd.forward(request,response);
		}else {
			response.sendRedirect("error.html");
		}
	}

	
}
