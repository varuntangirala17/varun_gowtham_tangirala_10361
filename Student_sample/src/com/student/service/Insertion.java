package com.student.service;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.student.bean.Address;
import com.student.bean.StudentBean;
import com.student.dao.StudentInsertiondao;

/**
 * Servlet implementation class Insertion
 */
@WebServlet("/Insertion")
public class Insertion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Insertion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//this method gets information from html page and sents to insertion dao class 
		String name=request.getParameter("name");
		String age=request.getParameter("age");
		String student_id=request.getParameter("id");
		int age1=Integer.parseInt(age);
		int stud_id=Integer.parseInt(student_id);
		String city=request.getParameter("city");
		String state=request.getParameter("state");
		String pinCode=request.getParameter("pinCode");
		int pinCode1=Integer.parseInt(pinCode);
		StudentBean student=new StudentBean();
		Address add=new Address();
		student.setStud_id(stud_id);
		student.setName(name);
		student.setAge(age1);
		add.setCity(city);
		add.setState(state);
		add.setPinCode(pinCode1);
		student.setAddress(add);
		StudentBean student1=StudentInsertiondao.insertStudent(student);
		if(student1!=null) {
			student.getStud_id();
			student.getName();
			student.getAge();
			student.getAddress();
			request.setAttribute("student",student);
			RequestDispatcher rd=request.getRequestDispatcher("Insert.jsp");
			rd.forward(request,response);
		}else {
			response.sendRedirect("error.html");
		}
		
	}

}
