package com.student.service;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.student.bean.StudentBean;
import com.student.dao.StudentSearchdao;

/**
 * Servlet implementation class StudentSearch
 */
@WebServlet("/StudentSearch")
public class StudentSearch extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentSearch() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		// it gets id from html page and sends to dao and gets the infromatiomn from database and diplays the information 
		String student_id=request.getParameter("id");
		int stud_id=Integer.parseInt(student_id);
		StudentBean student=StudentSearchdao.searchStudent(stud_id);
		request.setAttribute("student", student);
		RequestDispatcher rd=request.getRequestDispatcher("search_result.jsp");
		rd.forward(request,response);
	}


}
