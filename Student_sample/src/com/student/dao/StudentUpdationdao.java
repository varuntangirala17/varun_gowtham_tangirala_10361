package com.student.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.student.bean.Address;
import com.student.bean.StudentBean;
import com.student.util.DBConnection;
/**
 * This class is useful for updating student information
 * @author IMVIZAG
 *
 */
public class StudentUpdationdao {
	
  /**
   * this method connects with database and update record and sends result to servlet 
   * @param id
   * @param name
   * @param age
   * @return
   */
	public static StudentBean updateStudent(StudentBean student ) {
		
		Address add=student.getAddress();
		Connection conn=DBConnection.getDBConnection();
		try {
		Statement stmt=conn.createStatement();
		ResultSet rs=stmt.executeQuery("select * from student");
		while(rs.next()) {
			if(rs.getInt(1)==student.getStud_id()) {
		// create the my sql insert prepared statement
		String sql="update student set name=?,age=?,city=?,state=?, pincode=? where stud_id=?";
		
		PreparedStatement preparedStmt;
		
		preparedStmt = conn.prepareStatement(sql);
		preparedStmt.setInt(1, student.getStud_id());
		preparedStmt.setString(2, student.getName());
		preparedStmt.setInt(3, student.getAge());
		preparedStmt.setString(4, add.getCity());
		preparedStmt.setString(5, add.getState());
		preparedStmt.setInt(6, add.getPinCode());
		preparedStmt.executeUpdate();

	
		return student;
		
		}
		}
		}catch (SQLException e) {
			e.printStackTrace();
			}
	
		finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}}
		return null;
		
	}

}
