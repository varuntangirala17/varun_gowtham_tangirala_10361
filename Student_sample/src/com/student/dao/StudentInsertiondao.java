package com.student.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.student.bean.Address;
import com.student.bean.StudentBean;
import com.student.util.DBConnection;

/**
 * this class is useful for inserting student details
 * 
 * @author IMVIZAG
 *
 */
public class StudentInsertiondao {

	/**
	 * 
	 * this method is useful for inserting student details
	 * 
	 * @param id
	 * @param name
	 * @param age
	 * @return
	 */
	public static StudentBean insertStudent(StudentBean student) {

		Connection conn = DBConnection.getDBConnection();
		Address add=student.getAddress();
		// create the my sql insert prepared statement
		String sql = "insert into student values(?,?,?,?,?,?)";
		PreparedStatement preparedStmt;
		try {
			preparedStmt = conn.prepareStatement(sql);
			preparedStmt.setInt(1, student.getStud_id());
			preparedStmt.setString(2, student.getName());
			preparedStmt.setInt(3, student.getAge());
			preparedStmt.setString(4, add.getCity());
			preparedStmt.setString(5, add.getState());
			preparedStmt.setInt(6, add.getPinCode());
			preparedStmt.executeUpdate();
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		// sends information to sevice method
		return student;

	}

}
