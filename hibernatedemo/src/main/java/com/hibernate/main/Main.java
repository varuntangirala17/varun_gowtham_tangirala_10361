package com.hibernate.main;

import java.util.List;

import com.hibernate.DAO.CriteriaDemo;
import com.hibernate.entity.Address;
import com.hibernate.entity.Employees;
import com.hibernate.entity.Products;
/**
 * this class is used for demo purpose of criteria and one-one mapping
 * @author IMVIZAG
 *
 */
public class Main {
	public static void main(String[] args) {
		//creteria to display the product demo
		
		
		//setting attributes in product class 
		Products product=new Products();
		product.setProduct_id(101);
		product.setProduct_name("samsung");
		//calling the display method and taking into products list
		 List<Products> list= new CriteriaDemo().displayProducts();
		 System.out.println(list);
		 System.out.println("done");
        // one-one mapping example
		 
		 
		 //setting attributes in employee class
		 Employees e1=new Employees();    
		    e1.setEmp_name("varun1");  
		    e1.setEmail_id("varung1@gmail.com");
		    
		  //setting attributes in product class  
		    Address address1=new Address();    
		    address1.setCity("Ghaziabad11");    
		    address1.setState("U11");
		    
		   
		    e1.setAddress(address1);
		    address1.setEmployee(e1);
		    
		    //calling the insertproducts and saving into the the database
		    new CriteriaDemo().insertEmployees(e1);
		    
		  //calling the fetch method to fetch data from database
		    new CriteriaDemo().fetch();



	}

}
