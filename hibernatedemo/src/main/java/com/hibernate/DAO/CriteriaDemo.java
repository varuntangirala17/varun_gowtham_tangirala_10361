package com.hibernate.DAO;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.hibernate.entity.Address;
import com.hibernate.entity.Employees;
import com.hibernate.entity.Products;
import com.hibernate.util.HibernateUtil;
/**
 * This class is used perfrom dao operations using hibernate 
 * @author IMVIZAG
 *
 */
public class CriteriaDemo {
	/**
	 * this method is used to insert products 
	 * @param product
	 * @return
	 */
	public boolean insertProducts(Products product){
		//geting sessionfactory from util factory to create session
		SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
		
		//invoking session to perfrom operations
		Session session=sessionFactory.openSession();
		
		//begin transaction to commit the values
		Transaction t=session.beginTransaction();
		
		//saving the product
		session.save(product);
		
		//commting the transaction
	    t.commit();
	    
	    //closing the session class
		session.close();
		
		return true;
		
	}
	/**
	 * this method is used to insert employee 
	 * @param employee
	 * @return
	 */
	public boolean insertEmployees(Employees employee){
		//geting sessionfactory from util factory to create session
		SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
		
		//invoking session to perfrom operations
		Session session=sessionFactory.openSession();
		
		//begin transaction to commit the values
		Transaction t=session.beginTransaction(); 
		
		//saving th employee
		session.save(employee);
		
		//getting the address from employee
		Address address=employee.getAddress();
		
		//saving the address
		session.save(address);
		
		//commting the transaction
	    t.commit(); 
	    
	    //closing the session class
		session.close();
		
		return true;
		
	}
	/**
	 * this method is used to display products 
	 * @param product
	 * @return
	 */
	public List<Products> displayProducts(){
		//geting sessionfactory from util factory to create session
		SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
		
		//invoking session to perfrom operations
		Session session=sessionFactory.openSession();
		
		//using criteria to fetch the details from the database
		Criteria crit=session.createCriteria(Products.class);
		
		//taking into the list
		List<Products> list=crit.list();
		
		//closing the session class
		session.close();
		return list;
		
	}
		public void fetch(){
		//geting sessionfactory from util factory to create session
		SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
		
		//invoking session to perfrom operations
		Session session=sessionFactory.openSession();
		
		
		//getting the employee using get and fetch from the database
		Employees employee=session.get(Employees.class,1);
		
		System.out.println(employee.getEmp_id());
		System.out.println(employee.getEmp_name());
		System.out.println(employee.getEmail_id());
		
		//getting the address using get and fetch from the database
		Address address=employee.getAddress();
		
		System.out.println(address.getCity());
		System.out.println(address.getState());
		
		
		//saving the employee
		session.save(employee);
		
		//saving the address
		session.save(address);
		
		//closing the session class
		session.close();
		
		
	}
	

}
