package com.student.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.student.bean.StudentBean;
import com.student.dao.StudentSearchByNamedao;


/**
 * Servlet implementation class StudentSearchByName
 */
@WebServlet("/StudentSearchByName")
public class StudentSearchByName extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentSearchByName() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// it gets id from html page and sends to dao and gets the infromatiomn from database and diplays the information 
				String name=request.getParameter("user");
				List<StudentBean> student=StudentSearchByNamedao.searchStudent(name);
				PrintWriter pw=response.getWriter();
				pw.println("<html><body>");
				pw.println("<div align='center'>");
				if(student!=null) {
					pw.println("<h3>Students Table with name "+name+"</h3>");
					// display in the content in table format 
					pw.println("<table border='2'>");
					pw.println("<tr><th>student_id</th><th>name</th><th>age</th></tr>");
					Iterator<StudentBean> itr=student.iterator();
					while(itr.hasNext()) {
						StudentBean student1=(StudentBean) itr.next();
						pw.println("<tr><td>"+ student1.getStud_id()+"</td>"
								+ "<td>"+ student1.getName()+"</td>"
								+ "<td>"+ student1.getAge()+"</td></tr>");
						
					}
					
					pw.println("</table>");
				}
				else {
					RequestDispatcher rd=request.getRequestDispatcher("SearchStudentByName.html");
					rd.include(request,response);
					pw.println("<h2>Name not found</h2>");
				}
				pw.println("</div>");
				pw.println("</html></body>");
				//closing the printwriter
				pw.close();
			}
	



}
