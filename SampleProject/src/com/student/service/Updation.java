package com.student.service;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.student.dao.StudentUpdationdao;

/**
 * Servlet implementation class Updation
 */
@WebServlet("/Updation")
public class Updation extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Updation() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// it gets the information from html page and sends to updationdao

		String name = request.getParameter("name");
		String age = request.getParameter("age");
		String student_id = request.getParameter("id");
		int age1 = Integer.parseInt(age);
		int stud_id = Integer.parseInt(student_id);
		boolean result = StudentUpdationdao.updateStudent(stud_id, name, age1);
		PrintWriter pw = response.getWriter();
		pw.println("<html><body>");
		// it gets the result from database based on that it displays infromation on
		// html page
		if (result) {
			pw.println("<h2> Student record updated successfully</h2>");
		} else {
			pw.println("<h2>Id not found</h2>");
			RequestDispatcher rd = request.getRequestDispatcher("Updation.html");
			rd.include(request, response);
		}

		pw.println("</html></body>");
		// closing the printwriter
		pw.close();

	}

}
