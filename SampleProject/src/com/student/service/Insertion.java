package com.student.service;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.student.dao.StudentInsertiondao;

/**
 * Servlet implementation class Insertion
 */
@WebServlet("/Insertion")
public class Insertion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Insertion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		//this method gets information from html page and sents to insertion dao class 
		String name=request.getParameter("name");
		String age=request.getParameter("age");
		String student_id=request.getParameter("id");
		int age1=Integer.parseInt(age);
		int stud_id=Integer.parseInt(student_id);
		boolean result=StudentInsertiondao.insertStudent(stud_id,name,age1);
		PrintWriter pw=response.getWriter();
		// it gets the informmation from dao and then display information based on output 
		pw.println("<html><body>");
		if(result) {
			pw.println("<h2> Student record saved successfully</h2>");
		}
		else {
			pw.println("<h2>Record not saved</h2>");
		}
		
		pw.println("</html></body>");
		//closing the printwriter
		pw.close();
		
	}

}
