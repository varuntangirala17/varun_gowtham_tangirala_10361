package com.student.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.student.bean.StudentBean;
import com.student.dao.StudentDisplaydao;

/**
 * Servlet implementation class DisplayStudents
 */
@WebServlet("/DisplayStudents")
public class DisplayStudents extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayStudents() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//this method is used for collecting information from  display dao 
		List<StudentBean> studentList=StudentDisplaydao.displayStudent();
		PrintWriter pw=response.getWriter();
		// display in the content in table format 
		pw.println("<html><body><table border='2'>");
		pw.println("<tr><th>student_id</th><th>name</th><th>age</th></tr>");
		Iterator<StudentBean> itr=studentList.iterator();
		while(itr.hasNext()) {
			StudentBean student=(StudentBean) itr.next();
			pw.println("<tr><td>"+ student.getStud_id()+"</td>"
					+ "<td>"+ student.getName()+"</td>"
					+ "<td>"+ student.getAge()+"</td></tr>");
			
		}
		
		pw.println("</table></body></html>");
		//closing the printwriter
		pw.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
