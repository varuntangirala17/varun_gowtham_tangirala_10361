package com.student.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.student.bean.StudentBean;
import com.student.util.DBConnection;

/**
 * 
 * This class is useful for searching information based on id
 *
 */
public class StudentSearchdao {
	/**
	 * This method is useful for searching information based on id and returns info
	 * 
	 * @param id
	 * @return
	 */
	public static StudentBean searchStudent(int id) {

		Connection conn = DBConnection.getDBConnection();
		try {
			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery("select * from student");
			// gets the information form database and sets the objects and send the
			// information to servlet
			while (rs.next()) {
				StudentBean st = new StudentBean();
				if (rs.getInt(1) == id) {
					st.setStud_id(rs.getInt(1));
					st.setName(rs.getString(2));
					st.setAge(rs.getInt(3));
					return st;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return null;
	}
}
