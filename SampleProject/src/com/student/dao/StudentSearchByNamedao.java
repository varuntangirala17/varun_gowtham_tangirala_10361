package com.student.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.student.bean.StudentBean;
import com.student.util.DBConnection;

/**
 * 
 * This class is useful for searching information based on name
 *
 */
public class StudentSearchByNamedao {
	/**
	 * This method is useful for searching information based on name and returns
	 * info
	 * 
	 * @param name
	 * @return student bean object
	 */
	public static List<StudentBean> searchStudent(String name) {
		List<StudentBean> student= new ArrayList<StudentBean>();
		Connection conn = DBConnection.getDBConnection();
		try {
			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery("select * from student");
			// gets the information form database and sets the objects and send the
			// information to servlet
			while (rs.next()) {
				StudentBean st = new StudentBean();
				if (rs.getString(2).equalsIgnoreCase(name)) {
					st.setStud_id(rs.getInt(1));
					st.setName(rs.getString(2));
					st.setAge(rs.getInt(3));
					student.add(st);
				}
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return student;
	}

}
