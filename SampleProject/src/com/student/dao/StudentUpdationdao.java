package com.student.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.student.util.DBConnection;
/**
 * This class is useful for updating student information
 * @author IMVIZAG
 *
 */
public class StudentUpdationdao {
	
  /**
   * this method connects with database and update record and sends result to servlet 
   * @param id
   * @param name
   * @param age
   * @return
   */
	public static boolean updateStudent(int id,String name,int age ) {
	
		Connection conn=DBConnection.getDBConnection();
		try {
		Statement stmt=conn.createStatement();
		ResultSet rs=stmt.executeQuery("select * from student");
		while(rs.next()) {
			if(rs.getInt(1)==id) {
		// create the my sql insert prepared statement
		String sql="update student set name=?,age=? where stud_id=?";
		
		PreparedStatement preparedStmt;
		
		preparedStmt = conn.prepareStatement(sql);
		preparedStmt.setInt(3, id);
		preparedStmt.setString(1, name);
		preparedStmt.setInt(2, age);
		
		preparedStmt.executeUpdate();
		return true;
		
		}
		}
		}catch (SQLException e) {
			e.printStackTrace();
			}
	
		finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}}
		return false;
		
	}

}
