package com.student.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.student.util.DBConnection;

/**
 * this class is useful for inserting student details
 * 
 * @author IMVIZAG
 *
 */
public class StudentInsertiondao {

	/**
	 * 
	 * this method is useful for inserting student details
	 * 
	 * @param id
	 * @param name
	 * @param age
	 * @return
	 */
	public static boolean insertStudent(int id, String name, int age) {

		Connection conn = DBConnection.getDBConnection();

		// create the my sql insert prepared statement
		String sql = "insert into student(stud_id,name,age) values(?,?,?)";
		PreparedStatement preparedStmt;
		try {
			preparedStmt = conn.prepareStatement(sql);
			preparedStmt.setInt(1, id);
			preparedStmt.setString(2, name);
			preparedStmt.setInt(3, age);

			preparedStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// sends information to sevice method
		return true;

	}

}
