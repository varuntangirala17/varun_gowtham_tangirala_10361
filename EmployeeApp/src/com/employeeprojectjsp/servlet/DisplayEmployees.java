package com.employeeprojectjsp.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.employeeprojectjsp.bean.EmployeeDetails;
import com.employeeprojectjsp.dao.DisplayEmoployeesDao;

/**
 * Servlet implementation class DisplayEmployees
 */
@WebServlet("/DisplayEmployees")
public class DisplayEmployees extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DisplayEmoployeesDao displayEmoployeesDao = new DisplayEmoployeesDao();
		//calling the method which returns list and displaying that recieved list
		List<EmployeeDetails> employeeDetails = displayEmoployeesDao.displayEmployees();
		
		request.setAttribute("empdetails", employeeDetails);
		RequestDispatcher dispatcher = request.getRequestDispatcher("Display.jsp");
		dispatcher.include(request, response);
	}

}
