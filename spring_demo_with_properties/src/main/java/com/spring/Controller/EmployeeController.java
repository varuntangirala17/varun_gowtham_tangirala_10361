package com.spring.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.spring.Service.EmployeeService;
import com.spring.entity.Employee;

/**
 * This class is used to save employee details
 * @author VARUN
 *
 */
@Controller
public class EmployeeController {
	
	//declaring the variables
	@Autowired
	private EmployeeService employeeService;

	/**
	 * this method is used as a setter employee service method
	 * @param employeeService
	 */
	public void setEmployeeService(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	/**
	 * This method is used to save employee details 
	 * @return true if saved successfully 
	 */
	public Employee saveEmp(Employee employee) {
		// TODO Auto-generated method stub
		return  employeeService.save(employee);
		
	}
	

}
