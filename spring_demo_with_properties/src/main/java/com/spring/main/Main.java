package com.spring.main;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.spring.Controller.EmployeeController;
import com.spring.entity.Employee;
/**
 * This clas is used for getting output using spring annotation
 * @author Varun
 *
 */
public class Main {
	public static void main(String[] args) {
		
		//creating object for AnnotationConfigApplicationContext which implements ApplicationContext
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:com/spring/main/employee.xml");
		
		//calling the getBean method for getting the output
		EmployeeController controller=(EmployeeController) context.getBean(EmployeeController.class); 
		
		
		Employee employee=(Employee) context.getBean("abc"); 
		
		 //calling the save employee method
		 Employee emp= controller.saveEmp(employee);
		  
		  System.out.println("Emp saved: "+emp.getId());
		  
		  //closing the context
		  ((AbstractApplicationContext) context).close();
	}
	 
		
}
