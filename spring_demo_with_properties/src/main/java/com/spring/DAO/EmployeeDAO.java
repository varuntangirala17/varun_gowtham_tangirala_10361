package com.spring.DAO;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.entity.Employee;
import com.spring.util.HibernateUtil;

/**
 * This class is used to save employee details
 * @author VARUN
 *
 */
@Repository
public class EmployeeDAO {

	public static Employee create(Employee employee) {
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		//trasaction creation
		Transaction transaction =session.beginTransaction();
		
		//calling the save method 
		session.save(employee);
		
		//calling the commit method to save the data to database
		transaction.commit();
		 return employee;
	}

	
}
