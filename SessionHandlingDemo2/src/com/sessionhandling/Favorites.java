package com.sessionhandling;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Favorites")
public class Favorites extends HttpServlet {
	private static final long serialVersionUID = 1L;
	List<String> string;
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		HttpSession session = request.getSession(false);
		//if the session is null create new session
		if(session == null)
		{
			session = request.getSession();
			string=new ArrayList<String>();
			
		}
		else
		{
			//getting the hobbies from previous session and storing into the list
			string = (List<String>) session.getAttribute("hobbies");
		}
		// it gets hobbies from html page index.html
		String hobbies [] = request.getParameterValues("hobbies");
		
		//adding the hobbies from html file to string 
		for(String hobby:hobbies) {
			string.add(hobby);
		}
		//setting the hobbies attribute to session		
		session.setAttribute("hobbies", string);
		
		//creating requestdispatcher to include the request to display
		RequestDispatcher dispatcher = request.getRequestDispatcher("/Display");
		dispatcher.include(request, response);
		
	}

}
