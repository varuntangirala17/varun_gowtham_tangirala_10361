package com.sessionhandling;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Display
 */
@WebServlet("/Display")
public class Display extends HttpServlet {
	private static final long serialVersionUID = 1L;
    

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//creating the http session to get the sessio from shop servlet
		HttpSession session = request.getSession(false);
		
		//getting the attributes from shop servlet
		List<String> hobbies = (List<String>) session.getAttribute("hobbies");
		
		//Creating the printwriter object to write the data
		PrintWriter printWriter = response.getWriter();
		printWriter.println("<html><body>");
		printWriter.println("<div align='center'>");
		//print the string 
		for(String string : hobbies)
		{
			printWriter.println(string);
		}
		printWriter.println("</div>");
		printWriter.println("</html></body>");
		//setting the max inactive interval time limit of 5 seconds to expire the page
		session.setMaxInactiveInterval(5);
		
		printWriter.println("<a href=\"favorites.html\">go back to previous page</a>");
	}

}
