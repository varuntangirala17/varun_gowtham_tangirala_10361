package com.spring.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.DAO.EmployeeDAO;
import com.spring.entity.Employee;

@Service
public class EmployeeService {
	@Autowired
private EmployeeDAO employeedao;

public void setEmployeedao(EmployeeDAO employeedao) {
	this.employeedao = employeedao;
}

public Employee save() {
	Employee emp=EmployeeDAO.create();
	System.out.println(emp);
	return emp;
}
}
