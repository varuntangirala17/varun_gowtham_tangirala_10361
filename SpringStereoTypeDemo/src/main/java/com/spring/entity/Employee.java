package com.spring.entity;

import javax.persistence.Column;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/** 
 * This class is used to store employee details as an object
 * @author VARUN
 *
 */
@Entity
@Table(name="emp_tbl")
public class Employee {
	//declaring the variables
	@Id
 private int id;
	@Column
 private String name;
	@Column
 private String address;
	
	/**
	 * creating getters and setter methods to employee properties
	 * @return property
	 */
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
 
 
}
