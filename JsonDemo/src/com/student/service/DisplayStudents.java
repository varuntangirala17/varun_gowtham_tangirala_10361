package com.student.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.student.bean.StudentBean;
import com.student.dao.StudentDisplaydao;

/**
 * Servlet implementation class DisplayStudents
 */
@WebServlet("/DisplayStudents")
public class DisplayStudents extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson = new Gson();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayStudents() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//this method is used for collecting information from  display dao 
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		List<StudentBean> studentList=StudentDisplaydao.displayStudent();
		String studentJsonString = gson.toJson(studentList);
		out.print(studentJsonString);
		
	}

}
