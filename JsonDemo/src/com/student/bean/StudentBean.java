package com.student.bean;
/**
 * 
 * @author varun
 *This class is used for collecting student information 
 */
public class StudentBean {
private int stud_id;
private String name;
private int age;
private Address address;
/**
 * it consists of getters and setters methods of fields stud_id,name,age
 * @return
 */
public int getStud_id() {
	return stud_id;
}
public void setStud_id(int stud_id) {
	this.stud_id = stud_id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public Address getAddress() {
	return address;
}
public void setAddress(Address address) {
	this.address = address;
}

}
