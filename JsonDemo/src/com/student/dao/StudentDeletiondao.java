package com.student.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.student.bean.StudentBean;
import com.student.util.DBConnection;
/**
 * 
 * this class is used for deleting  student information in database 
 *
 */
public class StudentDeletiondao {

	public static StudentBean deleteStudent(int id ) {
	    /**
	     *  this method  is used for deleting  student information in database 
	     */
		Connection conn=DBConnection.getDBConnection();
		StudentBean student=new StudentBean();
		try {
		Statement stmt =conn.createStatement();
		// create the my sql insert prepared statement
		ResultSet rs=stmt.executeQuery("select * from student");
		// it stores the result in result set
		while(rs.next()) {
		String sql="delete from student where stud_id=?";
		PreparedStatement preparedStmt;
		preparedStmt = conn.prepareStatement(sql);
		preparedStmt.setInt(1, id);
		if(rs.getInt(1)==id) {
		preparedStmt.executeUpdate();
		student.setStud_id(id);
		student.setName(rs.getString(2));
		student.setAge(rs.getInt(3));
		// it gets the result 
		return student;
		}
		}
		}catch (SQLException e) {
			e.printStackTrace();
			}
		finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return null;
		
	}

}
