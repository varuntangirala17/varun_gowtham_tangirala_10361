package com.student.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.student.bean.Address;
import com.student.bean.StudentBean;
import com.student.util.DBConnection;

/**
 * This class is useful for collectiong information from database and sends the
 * info to servlet
 * 
 * @author IMVIZAG
 *
 */
public class StudentDisplaydao {
	/**
	 * This method is useful for collectiong information from database and sends the
	 * info to servlet
	 * 
	 * @return
	 */
	public static List<StudentBean> displayStudent() {
		List<StudentBean> studentList = new ArrayList<StudentBean>();
		Connection conn = DBConnection.getDBConnection();
		try {
			// it is useful for creating statment object
			Statement stmt = conn.createStatement();
			// create the my sql insert prepared statement
			ResultSet rs = stmt.executeQuery("select * from student");
			while (rs.next()) {
				// this is useful for storing inormation in arraylist and storing data to
				// arraylist
				StudentBean student = new StudentBean();
				Address add=new Address();
				student.setStud_id(rs.getInt(1));
				student.setName(rs.getString(2));
				student.setAge(rs.getInt(3));
				add.setCity(rs.getString(4));
				add.setState(rs.getString(5));
				add.setPinCode(rs.getInt(6));
				student.setAddress(add);
				studentList.add(student);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// returns to dao
		return studentList;

	}
}
