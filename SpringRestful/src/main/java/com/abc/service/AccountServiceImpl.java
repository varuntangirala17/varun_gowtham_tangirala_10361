package com.abc.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.DAO.AccounDAOImpl;
import com.abc.DAO.AccountDAO;
import com.abc.bean.Account;

/**
 * This class is used to fetch the adta from controller and send the data to dao
 * layer
 * 
 * @author IMVIZAG
 *
 */
@Service
public class AccountServiceImpl implements AccountService {
	/**
	 * This Variable is used to autoiwre the dao layer forrm service layer to remove
	 * the dependency
	 */
	@Autowired
	private AccountDAO accountDao;

	/**
	 * This method is used to search the account by using the id and send to dao
	 * layer
	 */
	@Override
	@Transactional
	public Account searchById(int accNo) {

		return accountDao.getAccountByAccNo(accNo);
	}

	/**
	 * This method is used to display the account details and send to dao layer
	 */
	@Override
	@Transactional
	public List<Account> displayAccountDetails() {

		return accountDao.displayDetails();
	}

	/**
	 * This method is used to add the account and send to dao layer
	 */
	@Override
	@Transactional
	public boolean addAccount(Account account) {

		return accountDao.createAccount(account);
	}

	/**
	 * This method is used to update the account and send to dao layer
	 */
	@Override
	@Transactional
	public Account updateAccount(Account account) {

		return accountDao.modifyAccount(account);
	}

	/**
	 * This method is used to delete the account and send to dao layer
	 */
	@Override
	@Transactional
	public void deleteAccount(Account account) {
		// TODO Auto-generated method stub
		accountDao.removeAccount(account);
	}

}
