package com.abc.DAO;

import java.util.List;

import com.abc.bean.Account;

public interface AccountDAO {

		Account getAccountByAccNo(int accno);

		boolean createAccount(Account account);
		
		Account modifyAccount(Account account);
		
		void removeAccount(Account account);
		
		List<Account> displayDetails();
	}



