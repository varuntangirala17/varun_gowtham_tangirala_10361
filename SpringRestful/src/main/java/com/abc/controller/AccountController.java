package com.abc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abc.bean.Account;
import com.abc.service.AccountService;
import com.abc.service.AccountServiceImpl;

@RestController
public class AccountController {
	@Autowired
	private AccountService accountService;
	
	@GetMapping("/accountsearch/{id}")
	public ResponseEntity<Account> searchAccount(@PathVariable("id") int id) {
		Account account=accountService.searchById(id);
		System.out.println(account);
		if(account!=null) {
			return new ResponseEntity<Account>(account,HttpStatus.OK);
		}
		else {
			//returns no content-204
			return new ResponseEntity<Account>(HttpStatus.NO_CONTENT);
		}
	}
	@PostMapping("/add")
	public ResponseEntity<?> createAccount(@RequestBody Account account) {
		
		boolean result=accountService.addAccount(account);
		
		if(result) {
		
			return new ResponseEntity<>("account is created", HttpStatus.OK);
		}
			return new ResponseEntity<>(HttpStatus.NOT_FOUND) ;
		}
	
	@PutMapping("/accountUpdation")
	public ResponseEntity<Account> updateAccount(@RequestBody Account account) {
		
		int accno =	account.getAccno();
		
		Account account1=accountService.searchById(accno);
		
		if(account1!=null) {
		
		 account=accountService.updateAccount(account);
		
		return new ResponseEntity<Account>(account,HttpStatus.OK);
		
		}
		return  new ResponseEntity<Account>(HttpStatus.NOT_FOUND) ;
	}
	
	@DeleteMapping("/accountdelete/{id}")
	public ResponseEntity<Account> deleteAccount(@PathVariable("id") int id) {
		Account account=accountService.searchById(id);
		if(account!=null) {
			accountService.deleteAccount(account);
			return new ResponseEntity<Account>(account,HttpStatus.OK);
		}
		return  new ResponseEntity<Account>(HttpStatus.NOT_FOUND) ;
	}
	
	@GetMapping("/displayAccounts")
	public ResponseEntity<List<Account>> displayAccounts(){
		
		List<Account> accounts = accountService.displayAccountDetails();
		
		if(accounts.size()!=0) {
			return new ResponseEntity<List<Account>>(accounts,HttpStatus.OK);
		}
		
		return new ResponseEntity<List<Account>>(HttpStatus.NO_CONTENT);
	}
}
