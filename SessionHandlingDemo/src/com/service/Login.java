package com.service;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Login
 */
@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 try{
		      response.setContentType("text/html");
		      
		      //Creating printwriter class object
		      PrintWriter pwriter = response.getWriter();
		      
		      // it gets name,password from html page index.html
		      String name = request.getParameter("userName");
		      String password = request.getParameter("userPassword");
		      
		      //printing the credentials
		      pwriter.print("Hello "+name+"<br>"+"<br>");
		      pwriter.print("Your Password is: "+password+"<br>"+"<br>");
		      
		      //httpsession creates unique session id 
		      HttpSession session=request.getSession();
		      
		      //setting the attributes using http session object
		      session.setAttribute("uname",name);
		      session.setAttribute("upass",password);
		      
		      /**setting the maxinactiveinterval  time of 5seconds to expire the page 
		       *  if the user clicks link after 5 seconds
		      **/
		      session.setMaxInactiveInterval(5);
		      
		      //Creating the link to another servlet to check the session handling function
		      pwriter.print("<a href='Welcome'>view details</a>");
		      
		      //closing the printwriter class
		      pwriter.close();
		    }catch(Exception exp){
		       System.out.println(exp);
		     }
	}

	
}
