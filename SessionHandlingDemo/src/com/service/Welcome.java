package com.service;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Welcome
 */
@WebServlet("/Welcome")
public class Welcome extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Welcome() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
		      response.setContentType("text/html");
		      
		      //creating the printwriter class obkject write the data in client side
		      PrintWriter pwriter = response.getWriter();
		      
		      //Linking the session to the Login servlet session
		      HttpSession session=request.getSession(false);
		      
		      //if session is not null getting the attributes using unique session id
		      if(session!=null) {
		      String myName=(String)session.getAttribute("uname");
		      String myPass=(String)session.getAttribute("upass");
		      pwriter.print("Name: "+myName+"<br>"+"<br>"+" Pass: "+myPass);
		      
		      }
		      else {
		    	  pwriter.println("session expired");
		    	  //expiring the if user enter directly welcome page url to unauthorize the page
		    	  session.invalidate();
		    	 
		      }
		      pwriter.close();
		  }catch(Exception exp){
		      System.out.println(exp);
		   }
	      }  
	


}
